
#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include "graph.hpp"
#include "algo.hpp"

#include <ctime>

/*
 * ./flows "graph path" "command 1" command 2"
 * Command list are : 
 *      print (print the graph)
 *      sources (print the sources of the graph"
 *      edge=id (selecting the edge at id (in the edge set, can be found by using print)
 *      node=id (selecting the node at id (in the node set, can be found by using print)
 *      epsilon=float (Assigning epsilon that is used during convergence)
 *      inputs (print the inputs of the selected node).
 *      outputs (print the outputs of the selected node).
 *      ancestors (print the ancestor edge of the selected edge)
 *      boundary (print the boundary of the selected edge)
 * 	decompose (prints the decomposition of the selected edge)
 * 
 * */

int main( int argc, const char* argv[] )
{
    if(argc==1)
    {
        std::cerr << "Error : No graph file specified, use ./flow <graphfile> "<< std::endl;
        exit(0);
    }
    std::cout << "Loading " << argv[1] << std::endl;
    std::clock_t startTime = std::clock();

    Graph *graph = new Graph(argv[1]);
    graph->InitAdjacencymatrix(); //Initialisation de la matrice d'adjacence. //Complexity : O(N*M)
    int id_edge_of_intrest = 0; //this id will be used as input of different algos that need an edge in input
    int id_node_of_intrest = 0; //this id will be used as input of different algos that need a node in input
    float epsilon=0.0001; //The default epsilon value
    std::cout << "Loading finished in "<<(std::clock() - startTime)/(double) CLOCKS_PER_SEC << "s" << std::endl;
    
    int instruction = 2;
    while(instruction < argc)
    {
        /* print command : print the graph structure (nodes, edges) */
        if(strcmp (argv[instruction], "print") == 0) 
        {
            std::cout << "***Printing graph : " << std::endl;
            Graph::PrintGraph(graph);
        }
        /* source command : prints the source nodes of the graph */
        else if(strcmp(argv[instruction], "sources") == 0)
        {
            startTime = std::clock();
            
            std::cout << "***Sources of the graph : " << std::endl;
            std::vector <const Node*>* sources = Algo::GetSources(graph);
            Node::Print(sources);
            
            std::cout << "Sources instruction finished in "<<(std::clock() - startTime)/(double) CLOCKS_PER_SEC << "s" << std::endl;
    
        }
        else if(strstr(argv[instruction], "edge=") != NULL)
        {
           sscanf(argv[instruction], "edge=%d", &id_edge_of_intrest);
           std::cout << "***Assigning the edge of interest to : " << id_edge_of_intrest << std::endl;
        }
        else if(strstr(argv[instruction], "node=") != NULL)
        {
             sscanf(argv[instruction], "node=%d", &id_node_of_intrest);
             std::cout << "***Assigning the node of interest to : " << id_node_of_intrest << std::endl;
        }
        else if(strstr(argv[instruction], "epsilon=") != NULL)
        {
            sscanf(argv[instruction], "epsilon=%f", &epsilon);
           std::cout << "***Assigning epsilon to : " << epsilon << std::endl;
        }
        
        /*inputs command : used to print the input edges of the node of interest */
        else if(strcmp(argv[instruction], "inputs") == 0)
        {
            startTime = std::clock();
            Node *node_of_interest = &graph->GetNodeSet()[id_node_of_intrest]; //On prend un noeud à examiner
            
            std::cout << "Inputs of node ("<< id_node_of_intrest << ") are : ";
            std::vector<const Edge* > *inputs = graph->GetInputs (node_of_interest);
            Edge::Print(inputs);
            std::cout << "inputs finished in "<<(std::clock() - startTime)/(double) CLOCKS_PER_SEC << "s" << std::endl;
            
        }
        /*outputs command : used to print the output edges of the node of interest */
        else if(strcmp(argv[instruction], "outputs") == 0)
        {
            startTime = std::clock();
            Node *node_of_interest = &graph->GetNodeSet()[id_node_of_intrest]; //On prend un noeud à examiner
            
            std::cout << "Outputs of node ("<< id_node_of_intrest << ") are : ";
            std::vector<const Edge* > *outputs = graph->GetOutputs (node_of_interest);
            Edge::Print(outputs);
            std::cout << "outputs finished in "<<(std::clock() - startTime)/(double) CLOCKS_PER_SEC << "s" << std::endl;
            
        }
        /*ancestors command : used to print the ancestor edges of the edge of interest */
        else if(strcmp(argv[instruction], "ancestors") == 0)
        {
            startTime = std::clock();
            Edge* edgeOfInterest = &graph->GetEdgeSet()[id_edge_of_intrest];
            std::vector<const Edge* > *Ancestors = Algo::GetAncestors(graph, edgeOfInterest);
            std::cout << "ancestors of "; Edge::Print(edgeOfInterest) ; std::cout << "are : " << std::endl;
            Edge::Print(Ancestors);
            std::cout << "ancestors finished in "<<(std::clock() - startTime)/(double) CLOCKS_PER_SEC << "s" << std::endl;
            
        }

        else if(strcmp(argv[instruction], "decompose") == 0)
        {
            startTime = std::clock();
            
            
            Edge* edgeOfInterest = &graph->GetEdgeSet()[id_edge_of_intrest];
            
            std::cout << "Decomposition of the flow of "; Edge::Print(edgeOfInterest) ; std::cout << " with epsilon = " << epsilon << std::endl;
            
            Formula * formula_f = Algo::FormulaExploration(graph, edgeOfInterest);	    
	    
	    std::map<std::string , float> * decomposed_map = Algo::FactorizeFormula(graph, formula_f, epsilon);
	    //Algo::Print(decomposed_map, edgeOfInterest);

            std::cout << "decomposition finished in "<<(std::clock() - startTime)/(double) CLOCKS_PER_SEC << "s" << std::endl;
        }
        /*boundary command : used to print the boundary of the edge of interest */
        else if(strcmp(argv[instruction], "boundary") == 0)
        {
            startTime = std::clock();
            Edge* edgeOfInterest = &graph->GetEdgeSet()[id_edge_of_intrest];
            std::vector<const Edge* > *EdgeBoundary = Algo::GetEdgeBoundary(graph, edgeOfInterest);
            std::cout << "boundary of "; Edge::Print(edgeOfInterest) ; std::cout << "are : " << std::endl;
            Edge::Print(EdgeBoundary);
            std::cout << "boundary finished in "<<(std::clock() - startTime)/(double) CLOCKS_PER_SEC << "s" << std::endl;
            
        }        
        else        
            std::cerr << "Igonoring command (unknown) : "<< argv[instruction] << std::endl;
        

        instruction++;
    }
    
    
    
    delete (graph);
    
    exit(1);
}