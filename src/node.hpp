
#ifndef NODE_H
#define NODE_H

#include <string>
#include <vector>

#include "edge.hpp"
#include "flow_vector.hpp"
#include "conversion_matrix.hpp"


class FlowVector;
class ConversionMatrix;

class Node
{
    
    
public :
    
    Node();
    ~Node();
    
    std::string getLabel() const;
    void setLabel(std::string label);
    
    //These fields might be null if no algorithm modified/set them.
    std::vector<const Node*>* Parents;
    std::vector<const Node*>* Successors;
    std::vector<const Edge*>* Inputs;
    std::vector<const Edge*>* Outputs;        
    FlowVector* outputVector;
    FlowVector* inputVector;
    ConversionMatrix* conversionMatrix;
    
    int id;
    int mark;
    
    static void Print(std::vector <const Node*>*);
    
private :
    
    
    std::string label;
};

#endif