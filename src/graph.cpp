#include "graph.hpp"

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <cstdlib>


int Graph::getSizeColourSet()
{
    return this->SizeColoursSet;
}
int Graph::getSizeNodeSet()
{
    return this->SizeNodeSet;
}
int Graph::getSizeEdgeSet()
{
    return this->SizeEdgeSet;
}
Node* Graph::GetNodeSet()
{
    return this->NodeSet;
}
Edge* Graph::GetEdgeSet()
{
    return this->EdgeSet;
}
FlowVector* Graph::getOutputVector(Node * n)
{
    if(n)
    {
        if(n->outputVector != NULL)
            return n->outputVector;
        else
        {
            //TODO : Construction de l'input vector pour la premiere fois
            //Ici le input vector est simplement le inputs dans le même ordre, cependant il faut faire gafffe a la matrice de conv
            n->outputVector = new FlowVector (this->GetOutputs(n));
            
        }
    }
    return n->outputVector;
}
FlowVector* Graph::getInputVector(Node * n)
{
    if(n)
    {
        if(n->inputVector != NULL)
            return n->inputVector;
        else
        {
            //TODO : Construction de l'input vector pour la premiere fois
            //Ici le input vector est simplement le inputs dans le même ordre, cependant il faut faire gafffe a la matrice de conv
            n->inputVector = new FlowVector (this->GetInputs(n));
            
        }
    }
    return n->inputVector;
}
ConversionMatrix* Graph::getConversionMatrix(Node * n)
{
    if(n)
    {
        if(n->conversionMatrix)
            return n->conversionMatrix;
        else
        {
                //std::cout << "Warning : node (" << n->getLabel() << ") has no conversion matrix attached, program will generate a uniform matrix (not sure about this..) " << std::endl;
                //TODO : What is this default matrix.... check graph.pdf
            
                FlowVector * inputVect = this->getInputVector(n);
                FlowVector * outputVect = this->getOutputVector(n);
                
		//if(inputVect->dim() != 0 && outputVect->dim() != 0)
		
                //Creation de la conversion matrix :
                n->conversionMatrix = new ConversionMatrix(outputVect->dim(), inputVect->dim()); 
                n->conversionMatrix->makeUniformMatrix(outputVect, inputVect);
		
                return n->conversionMatrix; //NULL right now because no generation done
        }
        
    }
    
}
    
Node* Graph::GetNodeWithLabel(std::string label)
{ 
    Node* nodeset=this->NodeSet;
    if(nodeset!=NULL)
    {        
        for(int i =0; i < this->SizeNodeSet; i++)
        {
            if(nodeset[i].getLabel().compare(label) == 0)
                return &nodeset[i];
        }
    }    
    return NULL;
}
Graph::Graph(std::string filepath)
{
    
    //std::cout << "Debug: Graph constructor : " << filepath << std::endl;
    
    //Load from the file the colour set, node set and edges set.
    try
    {
        
        std::ifstream graph_file;
        graph_file.open(filepath.c_str(), std::ios::in);
        std::string line;
        
        getline(graph_file, line);
        sscanf(line.c_str(), "size_colours_set=%d", &this->SizeColoursSet);
        getline(graph_file, line);
        sscanf(line.c_str(), "size_node_set=%d", &this->SizeNodeSet);
        getline(graph_file, line);
        sscanf(line.c_str(), "size_edge_set=%d", &this->SizeEdgeSet);
        
        
        //Reading and creating nodes and edges one by one
        NodeSet = new Node[this->SizeNodeSet];
        EdgeSet = new Edge[this->SizeEdgeSet];
        int nodes_created = 0;
        int edges_created = 0;
        std::string left_token, right_data;
        while( getline(graph_file, line))
        {
           left_token = line.substr(0, line.find_first_of(' '));
           right_data = line.substr(line.find_first_of(' '), line.length());
        
           if(left_token.compare("node")==0) //We are reading a node
           {
              
               char label[64];
               sscanf(right_data.c_str(), "%s", label);
 
  
               NodeSet[nodes_created].setLabel(label);
               NodeSet[nodes_created].id = nodes_created;
               nodes_created++;
               
               //Creating the node
           }
           else if (left_token.compare("edge")==0) //We are reading an edge
           {   
               //Creating the edge
               
                Node* source;
                Node* destination;
                char labelSrc[64];
                char labelDst[64];
                int colour;
                float value;               
                
               sscanf(right_data.c_str(), "%s %s %d %f", labelSrc, labelDst, &colour, &value);
               source = this->GetNodeWithLabel(labelSrc); // O(n)
               destination = this->GetNodeWithLabel(labelDst); // O(n)
               
               EdgeSet[edges_created].setSource(source);
               EdgeSet[edges_created].setDestination(destination);
               EdgeSet[edges_created].setColour(colour);
               EdgeSet[edges_created].setValue(value);
               EdgeSet[edges_created].id = edges_created;
               
               edges_created++;
           }
           else //token unknown
           {
               //TODO : add the conversion matrix 
               std::cerr << "Error : token " << left_token << " is unknown " << std::endl;
           }
            
        }
        //We check if we created all the nodes and edges and nothing is missing
        if(nodes_created != this->SizeNodeSet && edges_created != this->SizeEdgeSet)
        {
            std::cerr << "Error : Nodes or edges missing in the file." << std::endl;
            throw "error";
        }
        
        graph_file.close();
        
        
        //TODO : Setting input vectors
        //TODO : Setting output vectors
        //TODO : Setting conversion matrix
        
    }catch(...)
    {
        std::cerr << "Error : Graph file loading error, exiting" << std::endl;
        exit(0);
    }
    
    this->ModelGetPredecessors = NULL;
    this->ModelGetSuccessors = NULL;
    this->ModelGetOutputs = NULL;
    this->ModelGetInputs = NULL;
    
    
    
    
}
Graph::~Graph()
{
    //std::cout << "Debug: Graph ~Graph() called" << std::endl;
    delete[] NodeSet;
    delete[] EdgeSet;
    
}

void Graph::InitAdjacencylist()
{
std::cerr << "Error : function not coded yet ! " << std::endl;
}
void Graph::InitAdjacencymatrix()
{
    if(Adjacencymatrix!=NULL)
        delete Adjacencymatrix;
    
    Adjacencymatrix = new MatrixAdjacent(this->NodeSet, this->EdgeSet, this->SizeNodeSet, this->SizeEdgeSet);
    
    this->ModelGetPredecessors = Adjacencymatrix;
    this->ModelGetSuccessors = Adjacencymatrix;
    this->ModelGetOutputs = Adjacencymatrix;
    this->ModelGetInputs = Adjacencymatrix;
}
void Graph::InitIncidencematrix()
{
    std::cerr << "Error : function not coded yet ! " << std::endl;
}

GraphRepresentation* Graph::GetAdjacencylist()
{
    return Adjacencylist;
}
GraphRepresentation* Graph::GetAdjacencymatrix()
{
    return Adjacencymatrix;    
}
GraphRepresentation* Graph::GetIncidencematrix()
{
    return Incidencematrix;   
}
    
std::vector<const Node*>* Graph::GetPredecessorsOf(Node *n) //To do
{
       try{
        if (ModelGetPredecessors!=NULL)
        {
            return ModelGetPredecessors->GetPredecessorsOf(n);
        }
        else
            throw "Error : no graph representation to be used in Graph::GetPredecessorsOf";
    }   
    catch(std::string e)
    {
        std::cerr << e << std::endl;
    }
    catch(...)
    {
        std::cerr << "Error : Graph::GetPredecessorsOf failed" << std::endl;
    }
}
std::vector<const Node*>* Graph::GetSuccessorsOf(Node *n)
{
    try{
        if (ModelGetSuccessors!=NULL)
        {
            return ModelGetSuccessors->GetSuccessorsOf(n);
        }
        else
            throw "Error : no graph representation to be used in Graph::GetSuccessorsOf";
    }   
    catch(std::string e)
    {
        std::cerr << e << std::endl;
    }
    catch(...)
    {
        std::cerr << "Error : Graph::GetSuccessors failed" << std::endl;
    }
}
std::vector<const Edge*>* Graph::GetOutputs(Node *n) //To do
{
        try{
        if (ModelGetOutputs!=NULL)
        {
            return ModelGetOutputs->GetOutputs(n);
        }
        else
            throw "Error : no graph representation to be used in Graph::GetOutputs";
    }   
    catch(std::string e)
    {
        std::cerr << e << std::endl;
    }
    catch(...)
    {
        std::cerr << "Error : Graph::GetOutputs failed" << std::endl;
    }
}
std::vector<const Edge*>* Graph::GetInputs(Node *n) //To do
{
       try{
        if (ModelGetInputs!=NULL)
        {
            return ModelGetInputs->GetInputs(n);
        }
        else
            throw "Error : no graph representation to be used in Graph::GetInputs";
    }   
    catch(std::string e)
    {
        std::cerr << e << std::endl;
    }
    catch(...)
    {
        std::cerr << "Error : Graph::GetInputs failed" << std::endl;
    }
}

void Graph::PrintGraph(Graph *g)
{
     std::cout << "Number of colours : " << g->getSizeColourSet() << std::endl;
     std::cout << "Node set size : " << g->getSizeNodeSet() << std::endl;
     std::cout << "Node edge size : " << g->getSizeEdgeSet() << std::endl;
     
     std::cout << "node set is : " << std::endl;
     Node* nodeSet = g->GetNodeSet();
     for(int i =0; i < g->getSizeNodeSet(); i++)
     {
         std::cout << "W(" << nodeSet[i].getLabel() << ") ="; ConversionMatrix::Print(g->getConversionMatrix(&nodeSet[i])) ; std::cout << std::endl;
     }
     std::cout << std::endl;
     
       std::cout << "edges set is : {" << std::endl;
     Edge* edgeSet = g->GetEdgeSet();
     for(int i =0; i < g->getSizeEdgeSet(); i++)
     {
         std::cout << i<< " : ( " << edgeSet[i].getSource()->getLabel() << " -> " << edgeSet[i].getDestination()->getLabel() << " c= " << edgeSet[i].getColour()  << " v=" << edgeSet[i].getValue()  << " ) " <<std::endl;
     }
     
     
     std::cout << "}" <<  std::endl;
     
}

