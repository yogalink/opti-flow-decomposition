
#include "graph.hpp"

#include <iostream>

Edge::Edge()
{

    this->Ancestors=NULL;
    this->EdgeBoundary=NULL;
    this->SourceNodes=NULL;
}
Edge::~Edge()
{

    
    if(this->Ancestors!=NULL)
        delete this->Ancestors;
    
    if(this->EdgeBoundary!=NULL)
        delete this->EdgeBoundary;
    
    if(this->SourceNodes!=NULL)
        delete this->SourceNodes;
}
     
void Edge::setSource(Node * src)
{
    this->source=src;
}
void Edge::setDestination(Node* dst)
{
    this->destination=dst;
}
   
void Edge::setColour(int c)
{
    this->colour=c;
}
    
void Edge::setValue(float v)
{
    this->value = v;
}
bool operator==(Edge const& a, Edge const& b)
{
    if(a.getSource()==b.getSource() && a.getDestination() == b.getDestination() && a.getColour()==b.getColour() && a.getColour() ==b.getColour())
        return true;
    else
        return false;
}
     
Node* Edge::getSource() const
{
    return this->source;
}
    
Node* Edge::getDestination() const
{
    return this->destination;
}
int Edge::getColour() const
{

    return this->colour;
}
float Edge::getValue() const
{
    return this->value;
}
std::string Edge::getLabel() const
{
    std::string label = this->getSource()->getLabel() + " -> " + this->getDestination()->getLabel();
    return label;
}
void Edge::Print(std::vector <const Edge*>* set)
{
    if(set!=NULL)
    {
        std::cout << '{';
        for (std::vector<const Edge*>::iterator it = set->begin() ; it != set->end(); ++it)
        {
            if(*it==NULL)
                std::cerr << "Error : print a null *Edge  " << std::endl;
            else
               std::cout <<  " ( " << (*it)->getSource()->getLabel() << " -> " << (*it)->getDestination()->getLabel() << " c= " << (*it)->getColour()  << " v=" << (*it)->getValue()  << " ) ";
        }
        std::cout << "}\n";

    } 
}

void Edge::Print(const Edge* edg, bool light_print)
{
    if(edg==NULL)
        std::cerr << "Error : print a null *Edge  " << std::endl;
    else
    {
        std::cout <<  " ( " << edg->getSource()->getLabel() << " -> " << edg->getDestination()->getLabel(); 
        
        if(light_print)
            std::cout << " )";
        else
            std::cout << " c= " << edg->getColour()  << " v=" << edg->getValue()  << " ) ";
    }   


    
}