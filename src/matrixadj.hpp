
#ifndef MATRIX_ADJ_H
#define MATRIX_ADJ_H

#include "graph_representation.hpp"

class MatrixAdjacent : public GraphRepresentation
{
public : 
    MatrixAdjacent(Node*, Edge*, int size_node, int size_edge); // O(n*n)
    ~MatrixAdjacent();
    
    
    std::vector<const Node*>* GetPredecessorsOf(Node *); //O(n)
    std::vector<const Node*>* GetSuccessorsOf(Node *); //(O(n)
    std::vector<const Edge*>* GetOutputs(Node *); 
    std::vector<const Edge*>* GetInputs(Node *);
    
    void Print();
    
private :
    Edge*** matrix;
    int size_node;
    int size_edge;
    
};
   

#endif 