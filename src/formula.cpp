#include <iostream>

#include "formula.hpp"

Formula::Formula(Edge* left_edge_output, ConversionMatrix* mat, FlowVector* right_inputs)
{
    this->ptr_edge_output = left_edge_output;
    this->ptr_flow_inputs = right_inputs;
    this->ptr_conv_matrix = mat;
}

Formula::~Formula()
{
}
Edge* Formula::getLeftTerm()
{
    return this->ptr_edge_output;
}
ConversionMatrix* Formula::getConvMat()
{
    return this->ptr_conv_matrix;
}
FlowVector* Formula::getFlowVect()
{
    return this->ptr_flow_inputs;
}

void Formula::print()
{
   
    Edge::Print(this->ptr_edge_output, true); std::cout << " = " << std::endl;
    ConversionMatrix::Print(this->ptr_conv_matrix); std::cout << " x " << std::endl;
    FlowVector::Print(this->ptr_flow_inputs); std::cout << std::endl;
     
}
