
#ifndef CONVERSION_MATRIX_H
#define CONVERSION_MATRIX_H

#include <string>
#include "flow_vector.hpp"

class FlowVector;

class ConversionMatrix
{
    
    
public :
    
    ConversionMatrix(int nb_lines, int nb_cols);
    ~ConversionMatrix();
    
    void static Print(ConversionMatrix * mat);
    void makeUniformMatrix(FlowVector* outputs, FlowVector* inputs); 
    ConversionMatrix* cloneLine(int i_line); // Returns a new conversion matrice that is a line in the current matrice (line at i_line)
    int getNbCols(); //Returns the number of colones in the matrice
    int getNbLines(); //Returns the number of lines in the matrice
    
    
    std::string*** getComponentCell();
    std::string    getCellLabel(int i, int j, std::string node); 
    void           setComponentAt(int i, int j, std::string c); 
    void 	   setValueAt(int i, int j, float v);
    float 	   getCellValue(int i, int j);

    
private :

    std::string*** componentCell;
    float** cellValues;
    int j_cols;
    int i_lines;
    
};

#endif