#include "node.hpp"

#include <iostream>
#include <vector>

Node::Node()
{
    this->Parents=NULL;
    this->Successors=NULL;
    this->Inputs=NULL;
    this->Outputs=NULL;
    this->outputVector=NULL;
    this->inputVector=NULL;
    this->conversionMatrix=NULL;
    
    id=-1;
}
Node::~Node()
{
    if(this->Parents!=NULL)
        delete this->Parents;
    
    if(this->Successors!=NULL)
        delete this->Successors;
    
    if(this->Inputs!=NULL)
        delete this->Inputs;
    
    if(this->Outputs!=NULL)
        delete this->Outputs;
    
     if(this->outputVector!=NULL)
        delete this->outputVector;
    
    if(this->inputVector!=NULL)
        delete this->inputVector;
    
    if(this->conversionMatrix!=NULL)
        delete this->conversionMatrix;
}

std::string Node::getLabel() const
{
    return this->label;
}

void Node::setLabel(std::string label)
{
    this->label = label;
}

void Node::Print(std::vector <const Node*>* array)
{
    if(array!=NULL)
    {
        std::cout << '(';
    for (std::vector<const Node*>::iterator it = array->begin() ; it != array->end(); ++it)
    {
        if(*it==NULL)
            std::cerr << "Error : print a null *Node  " << std::endl;
        else
            std::cout << ' ' << (*it)->getLabel() ;
    }
        std::cout << ")\n";

    }   
}