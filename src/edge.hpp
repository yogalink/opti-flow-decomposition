
#ifndef EDGE_H
#define EDGE_H

#include <vector>
#include <string>

#include "node.hpp"
#include "formula.hpp"

class Node;
class Formula;


class Edge 
{
public :
    Edge();
    ~Edge();

    void setSource(Node * src);
    void setDestination(Node* dst);
    void setColour(int c);
    void setValue(float v);
    
     Node* getSource() const;
     Node* getDestination() const;
     int getColour() const;
     float getValue() const;
     std::string getLabel() const;
     int * getMark() const;

     //std::vector<const Edge*>* Parents;
     std::vector<const Edge*>* Ancestors;
     std::vector<const Edge*>* EdgeBoundary;
     std::vector<const Node*>* SourceNodes;
     
     static void Print(std::vector <const Edge*>*);
     static void Print(const Edge* edg, bool light_print=false);
     
     int id; //This is the index of this edge in the graph edge set, can be used in const cases to acces and write in the edge set directly
     int mark; //Used to set marks on the edges during the different algos
     int mark_2; //Deuxieme champs de mark, la flemme d'utiliser un seul
     //int mark_3; 
     
     Formula* local_formula; //Expresses the flow of this edge as a scalar product of a conversion matrix and a right term representing an input flow vector.
     
private :
    Node* source;
    Node* destination;
    int colour;
    float value;
    
};

bool operator==(Edge const& a, Edge const& b); //Operation d'egalité, renvoi vrai ssi a et b ont la même source, la même destination la même couleure et la même valeure
#endif