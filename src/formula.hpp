#ifndef FORMULA_H
#define FORMULA_H

#include "formula.hpp"
#include "flow_vector.hpp"
#include "conversion_matrix.hpp"

#define FORMULA_UNDEFINED 0

class FlowVector;
class ConversionMatrix;
class Edge;

class Formula 
{
public :
    Formula(Edge* left_output, ConversionMatrix* mat, FlowVector* right_inputs);
    
    Edge* getLeftTerm();
    ConversionMatrix* getConvMat();
    FlowVector* getFlowVect();
    
    ~Formula();
    
    void print(); // Print the formula

	//Used by the convergence
	float delta;
	ConversionMatrix* constant;
	int mark;

private :
    Edge* ptr_edge_output;
    FlowVector* ptr_flow_inputs;
    ConversionMatrix* ptr_conv_matrix;
    
    
};


#endif 
