#ifndef FLOW_VECTOR_H
#define FLOW_VECTOR_H

#include "node.hpp"
#include "edge.hpp"

#include <vector>
#include <string>

class Edge;
class Node;

class FlowVector
{
    
    
public :
    
    FlowVector(std::vector<const Edge*>  *vect_nodes); // Create a flow vector from a std::vector, using the same order
    FlowVector(); //Init an empty vector
    ~FlowVector(); //Destructor
    int getIndexFor(Edge * e); //Returns the position of edge e inside the flow vector
    
    std::vector<const Edge*>* getFlowsPtr(); //Gets the flow vectr ptr for iteration
    
    static void Print (FlowVector * vect); // Prints the vect in params
    int dim(); //the dimension of the vector
    
        
private :
    
    std::vector<const Edge*>* flow_ptr_edge; //Array of ptr to nodes ordered as they appear in the flow FLOW_VECTOR_H
    
    
};

#endif