#include "algo.hpp"
#include <stack>
#include <stdexcept>
#include <iostream>
#include <map>
#include <set>
#include <utility>
#include <queue>

#include <iomanip>
#include <cmath>
#include <limits>

#include "flow_vector.hpp"
#include "conversion_matrix.hpp"


std::vector<const Edge*>* Algo::GetParents(Graph* graph, Edge* e)
{
    
   return graph->GetInputs(e->getSource());
}

std::vector<const Node*> GetSources(Graph* gr)
{
    
}
std::vector<const Edge*>* Algo::GetAncestors(Graph* graph, Edge* e)
{
     /*
      * ancestors : edge vector <- empty
      * 
      * For all Nodes in graph do
      *
      * Node->mark = 0; //zero is unmarked
      * end do //(graph.setMarksAll(0)
      * 
      * stack : stack <- empthy;
      * stack.push (e.getSource)
      * e.mark = 1 // Marked
      * 
      * while(stack not empty) do
      *     Node* node = stack.pop();
      *     forall(inputEdge : graph.getInputs(node))
      *     {
      *         ancestors.add(inputEdge);
      * 
      *         if(inputEdge.source.mark == 0)
      *         {
      *             inputEdge.source.mark=1;
      *             stack.push(inputEdge.source);
      *         }
      *
      * 
      *     }
      * end while
      * 
      * return ancestors
      * */
     
     ///Complexity of this algorithm : O(n+m)
    
     if(e->Ancestors==NULL)     
        e->Ancestors = new std::vector<const Edge*>();
     
     else
        e->Ancestors->clear();
     
        int unmarked = 0;
        int marked = 1;
        
        //Marquer tous les noeuds du graphs comme non visités (O(n))
        for(int i = 0; i < graph->getSizeNodeSet(); i++)
        {
            graph->GetNodeSet()[i].mark=unmarked;
        }
        
        //Stack de noeud qu'il reste à explorer
        std::stack<Node*> exploration_stack;
        exploration_stack.push(e->getSource()); //On ajoute la source de l'arc à la pile d'exploration_stack
        e->getSource()->mark=marked; //By marking the node, we won't add it to the exploration stack more than one time
        
        //Tant que il reste des noeuds à explorer 
        while(!exploration_stack.empty())
        {
            //Retreiving the next node to explore 
            Node* currentNode = exploration_stack.top();
            exploration_stack.pop();
           // std::cout << "exploring node " << currentNode->getLabel() << std::endl;
            
            //Retreiving the input edges of the current explored node
            std::vector<const Edge*>*inputs = graph->GetInputs(currentNode); //TODO : Complexity of this
            //For all inputs of the node
            for (int i = 0; i < inputs->size(); i++)
            {
                const Edge* input = inputs->at(i);
                e->Ancestors->push_back(input);
                //std::cout << "adding a new input to ancestors : "; Edge::Print(input); std::cout << std::endl;
                
                //If the  source of the input edge is unmarked, we marked it for exploration and add it to the exploration stack
                if(input->getSource()->mark==unmarked) 
                {
                    input->getSource()->mark=marked;
                    exploration_stack.push(input->getSource());
                }
                
            }
            
          
        }

     
     
     return e->Ancestors;
 
}

std::vector<const Node*>* Algo::GetSources(Graph* graph)
{
    
    std::vector<const Node*>* sources = new std::vector<const Node*>();
    
    for(int i = 0; i < graph->getSizeNodeSet(); i++)
    {
            if (graph->GetInputs(&graph->GetNodeSet()[i])->empty())
            {
                sources->push_back(&graph->GetNodeSet()[i]);
            }
                
    }
        
    return sources;
}

std::vector<const Edge*>* Algo::GetEdgeBoundary(Graph* graph, Edge* e)
{
    std::stack<Node*> exploration_nodes; //Ancestor nodes to explore
    std::stack<const Edge*> potential_edges; //Potential edges that belong to the boudary are added here in part I
   // std::vector<const Edge*> edge_boundary; ///TODO
    
    int unmarked = 0;
    int marked = 1;
    
    //Marquer tous les noeuds du graphs comme non visités (O(n))
    for(int i = 0; i < graph->getSizeNodeSet(); i++)    
            graph->GetNodeSet()[i].mark=unmarked;
    
        

    exploration_nodes.push(e->getSource()); //On ajoute la source de l'arc à la pile d'exploration_nodes
    e->getSource()->mark=marked; //By marking the node, we won't add it to the exploration stack more than one time
    
       
    //PART I 
    
    while(!exploration_nodes.empty())
    {
        //Retreiving the next node to explore 
        Node* currentNode = exploration_nodes.top();
        exploration_nodes.pop();
        
        //Adding the parent nodes to the exploration stack and mark the currentNode
          std::vector<const Edge*>*inputs = graph->GetInputs(currentNode); 
        for (int i = 0; i < inputs->size(); i++)
        {
            const Edge* input = inputs->at(i);

            //If the  source of the input edge is unmarked, we marked it for exploration and add it to the exploration stack
            if(input->getSource()->mark==unmarked) 
            {
                input->getSource()->mark=marked;
                exploration_nodes.push(input->getSource());
            }                   
        }
            
        //Adding the outputs of the current node that dont lead to a marked node(a.k.a potentialy in the boundary)
        std::vector<const Edge*>*outputs = graph->GetOutputs(currentNode);
        for (int i = 0; i < outputs->size(); i++)
        {
            const Edge* output = outputs->at(i);
            if(output->getDestination()->mark==unmarked && !(output==e)) //It is important to not add the edge of intereset (e)
                potential_edges.push(output);
        }  
    }
    
    //PART II
    //We iterate for all the potential_edges and see if their destination leads to an unmarked node, in that case the edge is part of the boundary
    
    if(e->EdgeBoundary==NULL)
        e->EdgeBoundary = new std::vector<const Edge*>();
    else
        e->EdgeBoundary->clear();
    
    while(!potential_edges.empty())
    {
        //Retreiving the next node to explore 
        const Edge* currentEdge = potential_edges.top();
        potential_edges.pop();
        if(currentEdge->getDestination()->mark==unmarked)
                e->EdgeBoundary->push_back(currentEdge);
        
    }
    
    
    //Adding the edge of interest into the boundary as defined in the graph.pdf file.
    e->EdgeBoundary->push_back(e);
    
    return  e->EdgeBoundary;
    
    
}

std::map<std::string , float>* Algo::FactorizeFormula(Graph*gr, Formula* f, float epsilon)
{
	
	std::queue<std::pair<Formula* , ConversionMatrix*> > S; //Formula, Constant that will converge

	S.push(std::pair<Formula* , ConversionMatrix*>(f,  f->getConvMat() ));
	
	std::map<std::string , float>* Decomposition = new std::map<std::string , float>();
	std::map<std::string , float>* ConvergenceMap = new std::map<std::string , float>();
	
        
        bool continuepush = true;
        int itact=0;
        while(!S.empty()) 
	{
	   itact++;

	   std::pair<Formula* , ConversionMatrix*> P = S.front();
	   S.pop();
	
	   std::vector<const Edge*>* input_edges = P.first->getFlowVect()->getFlowsPtr();
        //cas de base
	//std::cout << "input size = " << input_edges->size() << std::endl;
	   

            if(input_edges->size()==0)
            {
                try
                {
                    float lc = Decomposition->at(P.first->getLeftTerm()->getLabel());				
                    Decomposition->erase(P.first->getLeftTerm()->getLabel());
                    Decomposition->insert(std::pair<std::string, float>(P.first->getLeftTerm()->getLabel(), lc + P.second->getCellValue(0,0)));	
                    
                    if(P.second->getCellValue(0,0) < epsilon)
                    {
                        try
                        {
                            ConvergenceMap->erase(P.first->getLeftTerm()->getLabel());
                        }
                        catch (const std::out_of_range &oor)             
                        {
                        }
                        if(ConvergenceMap->size() == 0)
                            continuepush = false;
                    }
                }
                catch (const std::out_of_range &oor)             
                { 
                        Decomposition->insert(std::pair<std::string, float>(P.first->getLeftTerm()->getLabel(), P.second->getCellValue(0,0)));
                        ConvergenceMap->insert(std::pair<std::string, float>(P.first->getLeftTerm()->getLabel(), P.second->getCellValue(0,0)));
                }
		
		std::cout << itact+1 << " )\t";
                Algo::Print(Decomposition, f->getLeftTerm());
            }
		
	    else
	    {
		for (int i = 0; i < input_edges->size(); i++)
                {
                    Edge* i_input = &gr->GetEdgeSet()[input_edges->at(i)->id];

		    
		    Formula* sf = i_input->local_formula; //This is the right term of the formula
		    ConversionMatrix* Csf = sf->getConvMat();
		    ConversionMatrix* c = P.second;
		   
			    //ce qui est arrive * ce qui va arriver
			ConversionMatrix* nc;
			if(Csf->getNbCols()==0)
			{
				nc = c;			    
			}
			else
			{
			nc = Csf->cloneLine(0);
			    for (int x = 0; x < nc->getNbCols(); x++){
				    nc->setComponentAt(0, x, "v");
				    nc->setValueAt(0, x, c->getCellValue(0,i)*Csf->getCellValue(0,x));
			    }
			}
			if(continuepush)
				S.push(std::pair<Formula* , ConversionMatrix*> (sf,  nc ));

		    
		}
	    }
	}
	
	std::cout << itact << " edges pushed during the algorithm." << std::endl;
	return Decomposition;

}
void Algo::Print(std::map<std::string , float>* map, Edge* e)
{
      if (map == NULL)
	std::cout << "Error printing a null map." << std::endl;
      else
      {
              int i = 0;
	      std::cout << "l(" << e->getLabel() <<")= ";
	      for(std::map<std::string , float>::iterator it=map->begin(); it!=map->end(); ++it)
              {
                  i++;
                    std::cout<< /*std::fixed << std::setprecision(5) << */ it->second  << "*l(" << it->first << ")";
                    if (i<map->size())
                    std::cout<<" +\t";
              }
              
      }
      std::cout << std::endl;
}
Formula* Algo::FormulaExploration(Graph* gr, Edge* e)
{
    std::stack<Edge*> exploration_stack; //The exploration stack containing the edges that we have to explore..
    
    
    /***INIT PART ***/
    
    //These flags will be used during the run of decomposition algorithm to compute ancestors, edge-boundary and sources of edge e if true
    bool compute_ancestors = true; 
    bool compute_sources = true;
    bool compute_boundary = true;
    
    std::set<const Edge*> potentialBoundarySet = std::set<const Edge*>();
    if(e->EdgeBoundary==NULL && compute_boundary)
    {
        //e->EdgeBoundary = new std::vector<const Edge*>();        
    }
    else //The boundary was computed previously so no need to do it again
        compute_boundary = false;
        
    if(e->Ancestors==NULL && compute_ancestors)
        e->Ancestors = new std::vector<const Edge*>();
    else //The ancestor set was computed previously so no need to do it again
        compute_ancestors=false;
    
    if(e->SourceNodes==NULL && compute_sources)
        e->SourceNodes = new std::vector<const Node*>();
    else //The ancestor set was computed previously so no need to do it again
        compute_sources=false;
    
    //Marking all the edges as unexplored
    int mark_unexplored = 0;
    int mark_explored = 1;
    int mark_in_boundary = 2;
    int mark_not_in_boundary = 3;
    
    for(int i = 0; i < gr->getSizeEdgeSet(); i++)    
    {
        gr->GetEdgeSet()[i].mark=mark_unexplored; // mark is used as a marking tool for exploration in general
        gr->GetEdgeSet()[i].mark_2=mark_not_in_boundary; // boundary is used a special mark to handle a boundary problem
    }
    //Marking all the nodes as unexplored
    for(int i = 0; i < gr->getSizeNodeSet(); i++)    
        gr->GetNodeSet()[i].mark=mark_unexplored;
    
    
    //Pushing the first edge into the stack
    exploration_stack.push(e);
    e->mark = mark_explored;
    //Adding the edge of interest into his boundary
    if (compute_boundary)
        potentialBoundarySet.insert(e);
    
    
    
    
    /*** Main loop : inverted dfs using the exploration stack *****/
    while(!exploration_stack.empty())
        {
            //Retreiving the next edge to explore 
            Edge* currentEdge = exploration_stack.top();
            exploration_stack.pop();
            
            Node* source_n = currentEdge->getSource();
            FlowVector* inputVector = gr->getInputVector(source_n);
            FlowVector* outputVector = gr->getOutputVector(source_n);
            ConversionMatrix* conversionMat =gr->getConversionMatrix(source_n);
                    
            
            //For every edge e we have : local_formula(e) <- h(e) = inputVector(e). Later in the code, we push all edges in the inputVector that are unexplored for exploration
            //In the end we obtain a formula chain starting from e to the sources(e). Another algo for rec printing the final formula is used.
            if(conversionMat)
            {
                int i = currentEdge->id;
                int index_in_outputV = outputVector->getIndexFor(&gr->GetEdgeSet()[i]);
                ConversionMatrix * line_edge_matrice = conversionMat->cloneLine(index_in_outputV);
                //line_edge_matrice->setMetalLabel(index_in_outputV, source_n->getLabel());
                gr->GetEdgeSet()[i].local_formula = new Formula (&gr->GetEdgeSet()[i], line_edge_matrice, inputVector);
                
            }
            

            /**Exploring the inputs of the source node of the current edge ***/
            std::vector<const Edge*>* input_edges = inputVector->getFlowsPtr();
            
            if (compute_sources && input_edges->empty() &&  source_n->mark == mark_unexplored) //Job of finding the source nodes
            {
                e->SourceNodes->push_back (source_n);
                source_n->mark = mark_explored;
            }
            /*The next loop will explore all the input vector :
                                            O(degre_inputs(source_node)) */
                for (int i = 0; i < input_edges->size(); i++)
                {
                    Edge* i_input = &gr->GetEdgeSet()[input_edges->at(i)->id];
                    //Marking the edge i_input as explored
                    if( i_input->mark == mark_unexplored)
                    {
                        i_input->mark=mark_explored;
                        if (compute_ancestors)
                            e->Ancestors->push_back(i_input); //Job for finding the ancestor set of edges
                        //Adding the i_input edge into the exploration stack
                        exploration_stack.push(i_input);
                        
                        if (compute_boundary && i_input->mark_2 == mark_in_boundary) //Case where the edge is taken in the edge-boundary but we just found out that it is an ancestor.
                        {
                            i_input->mark_2 == mark_not_in_boundary;
                            potentialBoundarySet.erase(i_input);
                        }
                    }
                    
                    
                }
            
            if (compute_boundary)
            {
                /**Exploring the outputs of the source node of the current edge ***/
                std::vector<const Edge*>* output_edges = outputVector->getFlowsPtr();
                for (int i = 0; i < output_edges->size(); i++)
                {
                        Edge* i_output = &gr->GetEdgeSet()[output_edges->at(i)->id];
                        
                        if (i_output->mark == mark_unexplored && i_output->mark_2 == mark_not_in_boundary)
                        {
                            i_output->mark_2  = mark_in_boundary;
                            potentialBoundarySet.insert(i_output);
                        }
                        
                }
            }
 
        }
    
    
    
    if (compute_boundary) //Setting the final edge-boundary set
        e->EdgeBoundary = new std::vector<const Edge*>(potentialBoundarySet.begin(), potentialBoundarySet.end() );

    
    //TODO : move this part up
    
    //Formula * formule = new Formula();
    
    return e->local_formula;
}
void Algo::TrajanStronglyConnectedComponents(Graph *gr)
{
    
}
