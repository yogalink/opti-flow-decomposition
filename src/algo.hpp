
#ifndef ALGO_H
#define ALGO_H

#include <vector>
#include <map>

#include "graph_representation.hpp"
#include "graph.hpp"
#include "formula.hpp"

namespace Algo 
{
    std::vector<const Edge*>* GetParents(Graph* gr, Edge* e);// O(1) : Gets the parent edges of edge e.
    std::vector<const Edge*>* GetAncestors(Graph* gr, Edge* e); // O(n+m) : Gets the ancestors of edge e.
    std::vector<const Node*>* GetSources(Graph* gr); // O(n) Gets the sources of the graph
    std::vector<const Edge*>* GetEdgeBoundary(Graph* gr, Edge* e);  // (O(n+m) get the boundary edge of a specified edge e.
    void TrajanStronglyConnectedComponents(Graph *gr); // O(n+m) : Trajan's algorithm for finding the strongly connected componenents
    
    
    Formula* FormulaExploration(Graph* gr, Edge* e);
    std::map<std::string , float>* FactorizeFormula(Graph*gr, Formula* f, float epsilon);
    void Print(std::map<std::string , float>* , Edge* e); //This algo is print the formula f and all the referenced other formulas to build the final formula, it stops when reaching a source node as it is a componenent of the flow decomposition problem
    
    
    
};

#endif 