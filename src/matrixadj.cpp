#include "matrixadj.hpp"


MatrixAdjacent::MatrixAdjacent(Node* nodeset, Edge* edgeset, int size_node, int size_edge)
{
    this->size_node = size_node;
    this->size_edge = size_edge;
    
    this->matrix = new Edge**[size_node];        
    for(int i = 0; i < size_node; i ++)
    {
       this->matrix[i] = new Edge*[size_node];
       for(int j = 0; j < size_node; j++)
       {
           this->matrix[i][j]=NULL;
       }
    }
    
    for(int i = 0; i < size_edge; i++)
    {
        this->matrix[edgeset[i].getSource()->id][edgeset[i].getDestination()->id]=&edgeset[i];
    }
}
MatrixAdjacent::~MatrixAdjacent()
{
    for(int i = 0; i < size_node; i ++)
    {
       delete[] this->matrix[i];
       
    }
    delete[] this->matrix;
}
    
    
std::vector<const Node*>* MatrixAdjacent::GetPredecessorsOf(Node *n)
{
    if(n->Parents==NULL)
    {
       n->Parents = new std::vector<const Node*>();
        for(int i = 0; i < this->size_node; i++) 
        {
            Edge* found = this->matrix[i][n->id];
            if(found != NULL)
                n->Parents->push_back(found->getSource());
        }
    }
    return n->Parents;
}
std::vector<const Node*>* MatrixAdjacent::GetSuccessorsOf(Node *n)
{
    if(n->Successors==NULL)
    {
        n->Successors = new std::vector<const Node*>();
         for(int i = 0; i < this->size_node; i++)
        {
            Edge* found = this->matrix[n->id][i];
            if(found != NULL)
            {  
                n->Successors->push_back(found->getDestination());
            }
        }
            
    }
    return n->Successors;
}
std::vector<const Edge*>*  MatrixAdjacent::GetOutputs(Node *n)
{
    if(n->Outputs==NULL)
    {
        n->Outputs = new std::vector<const Edge*>();
        for(int i = 0; i < this->size_node; i++) 
        {
            Edge* found = this->matrix[n->id][i];
            if(found != NULL)
            {
                n->Outputs->push_back(found);
            }
        }
    }
    return n->Outputs;
}

std::vector<const Edge*>*  MatrixAdjacent::GetInputs(Node *n)
{
    if(n->Inputs==NULL)
    {
        n->Inputs = new std::vector<const Edge*>();
        for(int i = 0; i < this->size_node; i++) 
        {
            Edge* found = this->matrix[i][n->id];
            if(found != NULL)
            {
                n->Inputs->push_back(found);
            }
        }
    }
    return n->Inputs;
}


void MatrixAdjacent::Print()
{
    
}