#include "flow_vector.hpp"
#include <iostream>


FlowVector::FlowVector() 
{
    this->flow_ptr_edge = new std::vector<const Edge*>();
}


FlowVector::FlowVector(std::vector<const Edge*> * vect_nodes)
{
    
    this->flow_ptr_edge = new std::vector<const Edge*>( );
    
    
    for (int i = 0 ; i < vect_nodes->size(); i ++)
    {
       // this->flow_ptr_edge->assign(i, vect_nodes->at(i));
    this->flow_ptr_edge->push_back(vect_nodes->at(i));
        
    }
    

    
}

FlowVector::~FlowVector()
{
    if (this->flow_ptr_edge)
        delete this->flow_ptr_edge;
}


void FlowVector::Print (FlowVector * vect_nodes)
{
   std::vector<const Edge*> * ptr = vect_nodes->getFlowsPtr();
   

   if (ptr && ptr->empty())
   {
       std::cout << "vect vide";
   }
   if(ptr && !ptr->empty())
   {
       
        std::cout << "(";
        for (int i = 0 ; i < ptr->size(); i ++)
        {
 
            
            std::cout << ptr->at(i)->getLabel();
            if(i!=ptr->size()-1)
                std::cout << ",";        
        }
        std::cout << ")";
    }
}
int FlowVector::getIndexFor(Edge * e) //Returns the position of edge e inside the flow vector
{
    std::vector<const Edge*>* vector = this->getFlowsPtr();
    int pos = -1;
    for (int i = 0; i <= vector->size(); i++)
    {
        if(*vector->at(i) == (*e))
            return i;
    }
    
    std::cerr << "Error : edge provided can't be found in the flow vector something wrong..." << std::endl;
}
int FlowVector::dim()
{
    if(this->flow_ptr_edge)
        return flow_ptr_edge->size();
}
std::vector<const Edge*>* FlowVector::getFlowsPtr()
{
    return this->flow_ptr_edge;
}