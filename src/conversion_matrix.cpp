#include "conversion_matrix.hpp"
#include <iostream>
#include <string>
#include <sstream>

//i : nombre de ligne, j : nombre de colonne
ConversionMatrix::ConversionMatrix(int i , int j  )
{

    i_lines = i;
    j_cols = j;
    
    int i_max = this->getNbLines();
    int j_max = this->getNbCols();
    this->componentCell = new std::string**[i_max];
    this->cellValues = new float*[i_max];
    std::ostringstream s; 
    for (int i = 0; i < i_max; i++)
    {
        this->componentCell[i] = new std::string*[j_max];
        this->cellValues[i] = new float[j_max];
        
        for (int j = 0; j < j_max; j++){
                 
            s << "f(" << i << "," << j << ")";
            this->componentCell[i][j] = new std::string (s.str());
            s.str(std::string());
	    
	    this->cellValues[i][j] = -1;

        }
    }

}
void ConversionMatrix::makeUniformMatrix(FlowVector* outputs, FlowVector* inputs)
{
    int i_max = this->getNbLines();
    int j_max = this->getNbCols();
    
    std::vector<const Edge*>* outputs_vec = outputs->getFlowsPtr();
    std::vector<const Edge*>* inputs_vec = inputs->getFlowsPtr();
    
    std::ostringstream s; 
   /* if(i_max == 0 || j_max == 0)
	    std::cout << "!!zero dimension matrix!!" << std::endl;
    */
    
    if(i_max == outputs->dim() && j_max == inputs->dim())
    {
	    
	float sum_output_flows = 0;
	for (int i = 0; i < i_max; i++)
		sum_output_flows += outputs_vec->at(i)->getValue();
	
	    
        for (int i = 0; i < i_max; i++)
        {
            for (int j = 0; j < j_max; j++)
            {
                s << "v(" << outputs_vec->at(i)->getDestination()->getLabel() << "," << inputs_vec->at(j)->getSource()->getLabel() << ")";
                this->componentCell[i][j] = new std::string (s.str());
                s.str(std::string());
		
		//Computing the unifor matrix cell by cell
		this->cellValues[i][j] = outputs_vec->at(i)->getValue()/sum_output_flows;
		//std::cout<<this->cellValues[i][j]<< std::endl;
            }
        }
    }
    else
        std::cerr << "Error dimension of input/output vector not matching with dim of the conversion matrix" << std::endl;
    

}
ConversionMatrix::~ConversionMatrix()
{
    if(this->componentCell)
    {
        int i_max = this->getNbLines();
        for (int i = 0; i < i_max; i++)
        {
            delete[] this->componentCell[i];
        }
        delete[] this->componentCell;
    }
    
    if(this->cellValues)
    {
        int i_max = this->getNbLines();
        for (int i = 0; i < i_max; i++)
        {
            delete[] this->cellValues[i];
        }
        delete[] this->cellValues;
    }
    
}
/*
std::string ConversionMatrix::getMetalLabel()
{
    std::ostringstream s; 
    s<<"W("<<meta_node<<",i="<<meta_i<<",j="<< meta_j <<")";

    return s.str();
    
}*/
std::string    ConversionMatrix::getCellLabel(int i, int j, std::string node)
{
    std::ostringstream s; 
    s<<"W("<<node<<","<< *this->componentCell[i][j] << ")";

    return s.str();
}

void 	   ConversionMatrix::setValueAt(int i, int j, float v)
{
	this->cellValues[i][j] = v;
}
float 	  ConversionMatrix:: getCellValue(int i, int j)
{
	return this->cellValues[i][j];
}
int ConversionMatrix::getNbCols()
{
    return this->j_cols;
}
int ConversionMatrix::getNbLines()
{
    return this->i_lines;
}
ConversionMatrix* ConversionMatrix::cloneLine(int i_line)
{
    int j_max = this->getNbCols();
    ConversionMatrix* cloned_line = new ConversionMatrix(1, j_max);
    std::string *** cells = this->getComponentCell();
        
    for (int j = 0; j < j_max; j++)
    {
        //std::cout << *cells[i_line][j] << " ";
        cloned_line->setComponentAt(0, j,  *cells[i_line][j]);
	cloned_line->setValueAt(0, j,  this->getCellValue(i_line,j));
    }
        
    return cloned_line;
    
}
std::string*** ConversionMatrix::getComponentCell()
{
    return this->componentCell;
}
void ConversionMatrix::setComponentAt(int i, int j, std::string c)
{
    if (this->componentCell)
    {
        if(this->componentCell[i][j])
            delete this->componentCell[i][j];
        
        this->componentCell[i][j] = new std::string(c);
    }
}
void ConversionMatrix::Print(ConversionMatrix * mat)
{
    if(mat)
    {
    int i_max = mat->getNbLines();
    int j_max = mat->getNbCols();
    std::string *** cells = mat->getComponentCell();
    std::cout <<"("<<i_max<<"*"<<j_max<<")"<<std::endl;
    for (int i = 0; i < i_max; i++)
    {
        std::cout << "| ";
        for (int j = 0; j < j_max; j++)
        {
            std::cout << *cells[i][j] << "=" << mat->getCellValue(i,j) << " ";
        }
        std::cout << "|";
        if (i_max!=1) //so that a line matrix won't do a \n
            std::cout << std::endl;
    }
    }
    else
        std::cout << " Error : matrice NULL " << std::endl;
    
}
