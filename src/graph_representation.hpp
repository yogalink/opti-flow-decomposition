#ifndef GRAPH_REPRESENTATION_H
#define GRAPH_REPRESENTATION_H

#include <vector>

#include "node.hpp"
#include "edge.hpp"

class GraphRepresentation
{
public :
    
    
    virtual std::vector<const Node*>* GetPredecessorsOf(Node *) = 0;
    virtual std::vector<const Node*>* GetSuccessorsOf(Node *) = 0;
    virtual std::vector<const Edge*>* GetOutputs(Node *) = 0;
    virtual std::vector<const Edge*>* GetInputs(Node *) = 0;
    
    virtual void Print() = 0;
    //Acces functions
};

#endif