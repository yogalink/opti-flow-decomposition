
#ifndef GRAPH_H
#define GRAPH_H

#include <string>
#include <vector>

#include "node.hpp"
#include "edge.hpp"

#include "graph_representation.hpp"
#include "matrixadj.hpp"


#include "flow_vector.hpp"
#include "conversion_matrix.hpp"


/**
 * Graph is the structure defining a colored flow graph 
 * 
 * 
 * **/
class Graph
{
public :

   
    Graph(std::string filepath);  
    ~Graph();
    
    static void PrintGraph(Graph *g);
    
    int getSizeColourSet();
    int getSizeNodeSet();
    int getSizeEdgeSet();
    
    Node* GetNodeSet();
    Edge* GetEdgeSet();
     
    Node* GetNodeWithLabel(std::string label); /* Complexity O(n)
                                                * \return the first Node* that has that label.
                                                * Parcours de NodeSet jusqu'a le trouver
                                                *NULL si aucun noeud n'a été trouvé */
    
    
    
    
    void InitAdjacencylist();
    void InitAdjacencymatrix();
    void InitIncidencematrix();
    
    GraphRepresentation* GetAdjacencylist();
    GraphRepresentation* GetAdjacencymatrix();
    GraphRepresentation* GetIncidencematrix();
    
    std::vector<const Node*>* GetPredecessorsOf(Node *);
    std::vector<const Node*>* GetSuccessorsOf(Node *);
    std::vector<const Edge*>* GetOutputs(Node *);
    std::vector<const Edge*>* GetInputs(Node *);
    
    FlowVector* getOutputVector(Node * n);
    FlowVector* getInputVector(Node * n);
    ConversionMatrix* getConversionMatrix(Node *);
    
    
private:
    
    GraphRepresentation* Adjacencylist;
    GraphRepresentation* Adjacencymatrix;
    GraphRepresentation* Incidencematrix;
    
    GraphRepresentation* ModelGetPredecessors;
    GraphRepresentation* ModelGetSuccessors;
    GraphRepresentation* ModelGetOutputs;
    GraphRepresentation* ModelGetInputs;
    
    int *ColoursSet; // A finite set of colours.
    int  SizeColoursSet; // The size of the colours set.
    
    
    Node* NodeSet; //A finite set of nodes 
    int   SizeNodeSet; // The size of node set, the number of nodes in the graph.
    
    Edge* EdgeSet; //A finite set of EdgeSet
    int   SizeEdgeSet; // The size of edge set, the number of edges in the graph.
    
    
    
    
};


#endif